import numpy as np
from scipy.signal import butter, lfilter, freqz
import matplotlib.pyplot as plt
import rospkg
import math

rospack = rospkg.RosPack()
path_of_shadow_hand_package = rospack.get_path("shadow_hand_contact_sensor")


def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype="low", analog=False)
    return b, a


def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y


# Filter requirements.
order = 6
fs = 100.0  # sample rate, Hz
cutoff = 1.5  # desired cutoff frequency of the filter, Hz

# Get the filter coefficients so we can check its frequency response.
b, a = butter_lowpass(cutoff, fs, order)

# Plot the frequency response.
w, h = freqz(b, a, worN=8000)

# Demonstrate the use of the filter.
# First make some data to be filtered.
T = 5.0  # seconds
n = int(T * fs)  # total number of samples

total_force_array = np.load(
    path_of_shadow_hand_package + "/data/contact_names.npy", allow_pickle=True
)
non_zero_contact_sensor_names = []

for cnt_name in total_force_array.item().keys():
    tmp_sum = sum(total_force_array.item().get(cnt_name))
    if tmp_sum != 0:
        print("non-zero contact sensors: ", cnt_name, "and the sum: ", tmp_sum)
        non_zero_contact_sensor_names.append(cnt_name)


def low_pass_plot(contact_name):
    if not isinstance(contact_name, (list, tuple)):
        contact_name = [contact_name]

    for index, con in enumerate(contact_name):
        data = total_force_array.item().get(con)
        t = range(0, len(data))
        y = butter_lowpass_filter(data, cutoff, fs, order)
        plt.figure(index)
        plt.subplot(3, 1, 1)
        plt.plot(
            0.5 * fs * w / np.pi,
            20 * np.log10(np.abs(h)),
            "b",
            label="filter-order: " + str(order),
        )
        plt.plot(cutoff, -3, "ko", label="cut-off: " + str(cutoff))
        plt.axvline(cutoff, color="k")
        plt.xlim(0, 0.5 * fs)
        plt.title("Lowpass Filter Frequency Response of " + con)
        plt.xlabel("Frequency [Hz]")
        plt.ylabel("Gain [dB]")

        plt.legend()
        plt.grid()

        plt.subplot(3, 1, 2)
        h_Phase = np.arctan2(np.imag(h), np.real(h))
        plt.plot(0.5 * fs * w / np.pi, h_Phase)
        plt.axvline(cutoff, color="k")
        plt.xlim(0, 0.5 * fs)
        plt.xlabel("Frequency [Hz]")
        plt.ylabel("Phase Angle [radian]")
        plt.legend()
        plt.grid()

        plt.subplot(3, 1, 3)
        plt.plot(t, data, "b-", label="data", alpha=0.3)
        plt.plot(t, y, "g-", linewidth=2, label="filtered data")
        plt.xlabel("Timesteps")
        plt.ylabel("$\sum$Total Force")
        plt.grid()
        plt.legend()

        plt.subplots_adjust(hspace=0.35)
    plt.show()


low_pass_plot("T_rfproximal_back_right")
