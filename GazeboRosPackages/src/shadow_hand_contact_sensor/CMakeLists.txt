cmake_minimum_required(VERSION 3.0.2)
project(shadow_hand_contact_sensor)



find_package(catkin REQUIRED COMPONENTS
  gazebo_msgs
  gazebo_ros
  roscpp
  std_msgs
  message_generation
)
find_package(gazebo REQUIRED)


add_service_files(
  FILES
  JointProperties.srv
  SetPIDParameters.srv
)

add_message_files(
  FILES
  shadowhand_link_pose.msg
  shadow_hand_contact_force.msg
)

generate_messages(
  DEPENDENCIES
  std_msgs
  geometry_msgs
)



catkin_package(
  INCLUDE_DIRS
  LIBRARIES ${PROJECT_NAME}
  CATKIN_DEPENDS message_runtime gazebo_msgs gazebo_ros roscpp std_msgs
  #DEPENDS system_lib
)

include_directories(include
  ${catkin_INCLUDE_DIRS}
  ${GAZEBO_INCLUDE_DIRS}
  ${TBB_INCLUDE_DIR}
)


add_library(shadow_hand_contact_controller SHARED src/shadow_hand_contact_controller.cpp)
target_link_libraries(shadow_hand_contact_controller ${GAZEBO_LIBRARIES})
add_dependencies(shadow_hand_contact_controller shadow_hand_contact_sensor_generate_messages_cpp)